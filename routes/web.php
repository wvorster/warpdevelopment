<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('tasks', 'TaskListController@ajaxList')->name('ajaxList');
Route::get('{id}/task', 'TaskListController@ajaxGet')->name('ajaxGet');
Route::post('task', 'TaskListController@ajaxPost')->name('ajaxPost');
Route::delete('{id}/task', 'TaskListController@ajaxDelete')->name('ajaxDelete');
Route::get('{id}/task/complete', 'TaskListController@ajaxComplete')->name('ajaxComplete');
