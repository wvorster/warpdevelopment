<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class TaskListController extends Controller
{
    function __construct()
    {

    }

    /**
     * Datatables response object
     *
     * @return JsonResponse
     */
    function ajaxList()
    {
        return datatables(Task::whereNull('completed_date'))
            ->addColumn('formatted_date', function($task) {
                return date("d-m-Y", strtotime($task->due_date));
            })
            ->addColumn('actions', function($task) {
                $complete = "<a class='on-default complete-row' data-toggle='modal' data-placement='top' title='Complete' data-target='#completeModal' onclick='completeModal(\"".route('ajaxComplete', $task->id)."\")'>Complete</a>";
                $edit = "<a class='on-default edit-row' data-toggle='modal' data-placement='top' title='Edit' data-target='#modalCreateEdit' onclick='populateModalCreateEdit(\"".route('ajaxGet', $task->id)."\")'>Edit</a>";
                $delete = "<a class='on-default delete-row' data-toggle='modal' data-placement='top' title='Delete' data-target='#deleteModal' onclick='deleteModal(\"".route('ajaxDelete', $task->id)."\")'>Delete</a>";
                return $complete." / ".$edit." / ".$delete;
            })
            ->rawColumns(['actions'])->toJson();
    }

    /**
     * Gets a single tasks data
     *
     * @param $id
     * @return JsonResponse
     */
    function ajaxGet($id)
    {
        return Task::find($id)->toJson();
    }

    /**
     * Saves or updates a task
     *
     * @param Request $request
     * @return JsonResponse
     */
    function ajaxPost(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:100',
                'body' => 'required',
                'due_date' => 'required|date',
                'priority' => 'required|boolean'
            ]
        );
        if ($validator->fails()) {
            return [
                'success' => false,
                'errors' => $validator->getMessageBag()->toJson()
            ];
        }

        if(empty($request->get('id'))) {
            $task = new Task();
        } else {
            $task = Task::find($request->get('id'));
        }

        $task->title = $request->get('title');
        $task->body = $request->get('body');
        $task->due_date = date('Y-m-d', strtotime($request->get('due_date'))); // need to store it for mysql format
        $task->priority = $request->get('priority');

        $task->save();

        return response()->json(['success' => true]);
    }

    /**
     * Soft deletes a task
     *
     * @param $id
     * @return JsonResponse
     */
    function ajaxDelete($id)
    {
        Task::find($id)->delete();
        return response()->json(['success' => true]);
    }

    /**
     * Marks a task as completed
     *
     * @param $id
     * @return JsonResponse
     */
    function ajaxComplete($id)
    {
        $task = Task::find($id);

        $task->completed_date = date("Y-m-d");

        $task->save();

        return response()->json(['success' => true]);
    }
}
