<?php

namespace App\Console;

use App\Task;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            $tasks = Task::where('due_date', date("Y-m-d"))->whereNull('completed_date')->get();

            if(!empty($tasks)) foreach($tasks as $task) {
                Mail::send(['text' => 'mail'], $task, function ($message) use ($task) {
                    $message
                        ->to(env('EMAIL_RECEIVER', 'wavorster@gmail.com'), 'Wade Vorster')
                        ->subject('Task ' . $task->title . ' due today.')
                        ->from('task@system', 'Task System')
                        ->setBody($task->body);
                });
            }
        })->dailyAt('01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
