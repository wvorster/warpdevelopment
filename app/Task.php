<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'body', 'due_date', 'priority',
    ];

    public function getDueDateAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
