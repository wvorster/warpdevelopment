@extends("layouts/app")

@section("header_styles")
    <link rel="stylesheet" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"  type="text/css" crossorigin="anonymous">

    <style type="text/css">
        .high-priority {
            color: red;
        }
    </style>
@stop

@section("content")
    <div class="row">
        <div class="col-md-12">
            <h3>My TODO List</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalCreateEdit" onclick="populateModalCreateEdit(null)">Create Task</button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table id="taskListTable" class="display">
                <thead>
                <tr>
                    <td>Title</td>
                    <td>Task</td>
                    <td>Due Date</td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2><strong>Note to Warp Development</strong></h2>
            <p>
                Laravel task management system using jQuery and Bootstrap 4.<br>
                Uses jQuery <a href="https://datatables.net/" target="_blank">DataTables</a> for table manipulation and <a href="https://gijgo.com/datepicker" target="_blank">gijgo</a> (Bootstrap date formatter).<br>
                Completely AJAX using JSON responses.<br>
                Server side validation using Laravel validation class.<br>
                Table control and manipulation by package <a href="https://datatables.yajrabox.com/" target="_blank">Laravel Yajra DataTables</a>.<br>
                This repo can be found on bitbucket at url: <a href="https://bitbucket.org/wvorster/warpdevelopment">https://bitbucket.org/wvorster/warpdevelopment</a><br>
                or a complete zip of this project can be found at <a href="http://tasklist.codelink.co.za/files/TaskList.zip">My Personal Server</a>.
                <strong>Note:</strong><br>
                I can do a lot with this still but I am sticking to specification and keeping this clean and neat.<br>
                Setting a cron job to this will run a scheduled task that will email the person set in the .env file at 01:00 all the due tasks for the day.<br><br>
                Regards<br>
                Wade Vorster
            </p>
        </div>
    </div>
    <div class="modal" id="modalCreateEdit">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="formCreateEdit">
                    <input type="hidden" name="id" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Create / Edit Task</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="input-form">
                            <label for="modalCreateEditTitle">Task Title</label>
                            <input type="text" id="modalCreateEditTitle" class="form-control" name="title" placeholder="Task Title">
                        </div>
                        <div class="input-form">
                            <label for="modalCreateEditBody">What to do?</label>
                            <textarea id="modalCreateEditBody" class="form-control" name="body" placeholder="What this task entails..."></textarea>
                        </div>
                        <div class="input-form">
                            <label for="modalCreateEditDueDate">Due Date</label>
                            <div class="input-group">
                                <input type="text" id="modalCreateEditDueDate" class="form-control" name="due_date" width="276" value="{{ date("d-m-Y") }}" readonly data-special="date">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-form">
                            <label for="modalCreateEditPriority">High Priority</label>
                            <input type="hidden" name="priority" value="0"><br>
                            <input type="checkbox" id="modalCreateEditPriority" class="form-control move-left" name="priority" value="1" style="width:40px;">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="deleteModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="deleteForm">
                    <input type="hidden" name="url" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this task?
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Delete</button><button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="completeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="completeForm">
                    <input type="hidden" name="url" value="">
                    <div class="modal-header">
                        <h4 class="modal-title">Complete</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to complete this task?
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary">Complete</button><button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section("footer_scripts")
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#modalCreateEditDueDate").datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mm-yyyy'
            });

            $("#taskListTable").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url': '{!! route('ajaxList') !!}',
                    'type': 'GET',
                },
                columns: [
                    { data: 'title', name: 'title' },
                    { data: 'body', name: 'body' },
                    { data: 'formatted_date', name: 'due_date' },
                    { data: 'due_date', name: 'due_date' },
                    { data: 'actions', name: 'actions', searchable: false, sortable: false},
                ],
                columnDefs: [
                    { "width": "150px", "targets": 0 },
                    { "width": "80px", "targets": 2 },
                    { "width": "150px", "targets": 4 },
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": false
                    }
                ],
                order: [
                    [3, "asc"],
                ],
                createdRow: function( row, data, dataIndex){
                    if( data['priority'] == 1){
                        $(row).addClass('high-priority');
                    }
                },
                autoWidth: false
            });

            $('#formCreateEdit').on('submit', function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{!! route('ajaxPost') !!}",
                    type: "POST",
                    data: $('#formCreateEdit').serialize(),
                    success: function (data, status) {
                        if (data.success === true) {
                            $("#modalCreateEdit .close").click();
                            $("#taskListTable").DataTable().ajax.reload();
                        } else {
                            let form = $("#formCreateEdit *").filter(':input').not(':button').not(':hidden');
                            let errors = $.parseJSON(data.errors);

                            form.each(function () {
                                if (errors.hasOwnProperty($(this).attr('name'))) {
                                    let error = "<span class='text-danger'>" + errors[$(this).attr('name')][0] + "<span>";
                                    if($(this).data("special") === "date") {
                                        $(this).parent().parent().after(error);
                                    } else {
                                        $(this).after(error);
                                    }
                                }
                            })
                        }
                    }
                });
            });

            $('#deleteForm').on('submit', function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: $("#deleteForm [name='url']").val(),
                    method: "DELETE",
                    contentType: 'application/json',
                    success: function(data) {
                        if(data.success !== true) {
                            alert("There was a problem deleting. Please try again later.");
                        }
                        $("#deleteModal .close").click();
                        $("#deleteForm [name='url']").val('');
                        $("#taskListTable").DataTable().ajax.reload();
                    },
                    error: function(request,msg,error) {
                        $("#deleteModal .close").click();
                        $("#deleteForm [name='url']").val('');
                        alert("There was a problem deleting. Please try again later.");
                    }

                })
            });

            $('#completeForm').on('submit', function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: $("#completeForm [name='url']").val(),
                    method: "GET",
                    contentType: 'application/json',
                    success: function(data) {
                        if(data.success !== true) {
                            alert("There was a problem completing. Please try again later.");
                        }
                        $("#completeModal .close").click();
                        $("#completeForm [name='url']").val('');
                        $("#taskListTable").DataTable().ajax.reload();
                    },
                    error: function(request,msg,error) {
                        $("#completeModal .close").click();
                        $("#completeForm [name='url']").val('');
                        alert("There was a problem completing. Please try again later.");
                    }

                })
            })

        });

        function populateModalCreateEdit(url) {
            $(".text-danger").remove();
            if(url == null) {
                $("#formCreateEdit").find('[name=id]').val('');
                $("#formCreateEdit").trigger('reset');
            } else {
                $.ajax({url: url,
                    success: function(data) {
                        data = $.parseJSON(data);
                        let form = $("#formCreateEdit *").filter(':input').not(':button').not('input[type="hidden"][name="priority"]');

                        form.each(function() {
                            if(data.hasOwnProperty($(this).attr('name'))) {
                                if($(this).attr("type") === "checkbox") {
                                    $(this).prop('checked', data[$(this).attr('name')]);
                                } else {
                                    $(this).val(data[$(this).attr('name')]);
                                }
                            }
                        })
                    }}
                );
            }
        }
        function deleteModal(url) {
            $("#deleteForm [name='url']").val(url);
        }
        function completeModal(url) {
            $("#completeForm [name='url']").val(url);
        }
    </script>
@stop
